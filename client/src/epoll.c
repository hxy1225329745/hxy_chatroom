//
// Created by 12253 on 2020/1/16.
//
#include <stdio.h>
#include <fcntl.h>
#include "epoll.h"

int epoll_init() {
    int epfd = epoll_create(EPOLL_SIZE);
    if(epfd < 0) { perror("epfd error");}

    printf("epoll created, epollfd = %d\n", epfd);

    return epfd;
}

void epoll_event_addfd(int epollfd, int fd, _Bool enable_et) {
    struct epoll_event ev;
    ev.data.fd = fd;
    ev.events = EPOLLIN;

    if (enable_et) {
        ev.events = EPOLLIN | EPOLLET;
    }

    epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev);

    fcntl(epollfd, F_SETFL, fcntl(epollfd, F_GETFD, 0)| O_NONBLOCK);

    printf("fd added to epoll!\n\n");
}
