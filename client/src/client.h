//
// Created by 12253 on 2020/1/15.
//

#ifndef EPOLL_SERVER_H
#define EPOLL_SERVER_H

#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <stdbool.h>
#include <unistd.h>
#include "arraylist.h"
#include "epoll.h"

#define SERVER_IP "127.0.0.1"
#define EXIT "exit"
#define BUF_SIZE 0xFFFF

struct Client{
    int sockfd;
    int pipe_fd[2];
    short port;
    int client_work_ok;
    int epfd;
};
typedef struct Client client;

#endif //EPOLL_SERVER_H
