//
// Created by 12253 on 2020/1/15.
//
#include "arraylist.h"

arraylist* arraylist_create() {
    arraylist* new_list = malloc(sizeof(arraylist));
    new_list->size = 0;
    new_list->capacity = 8;
    new_list->body = malloc(sizeof(void*) * 8);
    return new_list;
}

void arraylist_append(arraylist* l) {
    l->capacity = l->capacity * 2;
    l->body = realloc(l->body, sizeof(void*) * l->capacity);
}

int arraylist_append_ornot(arraylist *l) {
    if (l->size >= l->capacity - 2) {
        return 1;
    } else {
        return 0;
    }
}

void arraylist_add(arraylist *l, void* item) {
    int ret = arraylist_append_ornot(l);
    if (ret) {
        arraylist_append(l);
    }
    l->body[l->size++] = item;
}

void* arraylist_get(arraylist *l, unsigned int index) {
    return l->body[index];
}

void arraylist_remove(arraylist *l, unsigned int index) {
    int length = l->size - index - 1;
    memmove(l->body, l->body + index, sizeof(l->body) * length);
    l->size--;
}

int arraylist_find(arraylist *l, int content) {
    for (int i = 0; i < l->size; ++i) {
        if (content == *(int *)(l->body[i])) {
            return i;
        }
    }
    return -1;
}