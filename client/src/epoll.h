//
// Created by 12253 on 2020/1/16.
//

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>

#define EPOLL_SIZE 5000

int epoll_init();
void epoll_event_addfd(int epollfd, int fd, bool enable_et);

//EPOLL7_EPOLL_H
