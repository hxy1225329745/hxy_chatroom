//
// Created by 12253 on 2020/1/15.
//
#include "client.h"
#include <sys/epoll.h>

static client* client_init() {
    client* client_;
    client_ = malloc(sizeof(*client_));
    memset(client_, 0, sizeof(*client_));
    client_->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    client_->client_work_ok = 1;
    return client_;
}

static void server_connection(client *client_) {
    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof serv_addr);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(SERVER_IP);
    serv_addr.sin_port = htons(8888);

    if (client_->sockfd < 0) {
        perror("socket");
        exit(1);
    }

    if (connect(client_->sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("connect error");
        exit(-1);
    }

}

static void pipe_create(client *client_) {
    int pipe_fd[2];
    int ret = pipe(pipe_fd);
    if (ret < 0) {
        perror("pipe error");
        exit(-1);
    }
    printf("pipe[0]=%d,pipe[1]=%d", pipe_fd[0], pipe_fd[1]);
    client_->pipe_fd[0] = pipe_fd[0];
    client_->pipe_fd[1] = pipe_fd[1];
}

static void do_fork_strategy(client *client_) {
    printf("aaaaa\n");
    char message[BUF_SIZE];
    static struct epoll_event events[2];

    int pid = fork();
    if (pid < 0) {
        perror("fork error");
        exit(-1);
    } else if (pid == 0) {
        close(client_->pipe_fd[0]);
        printf("please input 'exit' to exit this chat room\n");

        while (client_->client_work_ok) {
            bzero(message, BUF_SIZE);
            fgets(message, BUF_SIZE, stdin);

            if (strncasecmp(message, EXIT, strlen(message)) == 0) {
                client_->client_work_ok = false;
            } else {
                if (write(client_->pipe_fd[1], message, strlen(message) - 1) < 0) {
                    perror("write error");
                    exit(-1);
                }
            }

        }
    } else {

        close(client_->pipe_fd[1]);

        while (client_->client_work_ok) {

            int epoll_evens_count = epoll_wait(client_->epfd, events, 2, -1);

            for (int i = 0; i < epoll_evens_count; ++i) {
                bzero(message, BUF_SIZE);

                if (events[i].data.fd == client_->sockfd) {
                    int ret = recv(client_->sockfd, message, BUF_SIZE, 0);
                    if (ret == 0) {
                        printf("Server closed connection: %d", client_->sockfd);
                        close(client_->sockfd);
                        client_->client_work_ok = 0;
                    } else {
                        printf("%s\n", message);
                    }

                } else {
                    int ret = read(events[i].data.fd, message, BUF_SIZE);
                    if (ret == 0) client_->client_work_ok = 0;
                    else {
                        send(client_->sockfd, message, BUF_SIZE, 0);
                    }
                }
            }
        }
    }
    if (pid) {
        close(client_->pipe_fd[1]);
        close(client_->sockfd);
    } else {
        close(client_->pipe_fd[0]);
        close(client_->sockfd);
    }
}

static void start_client(client *client_) {
    server_connection(client_);

    pipe_create(client_);

    int epfd = epoll_init(2);
    if (epfd < 0) {perror("epfd error"); exit(-1);}

    epoll_event_addfd(epfd, client_->sockfd, true);
    epoll_event_addfd(epfd, client_->pipe_fd[0], true);

    client_->epfd = epfd;

    do_fork_strategy(client_);
}

int main(int argc, char** argv) {
    client* client_;
    client_ = client_init();

    start_client(client_);
    return 0;
}