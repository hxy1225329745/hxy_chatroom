//
// Created by 12253 on 2020/1/16.
//

#ifndef EPOLL7_EPOLL_H
#define EPOLL7_EPOLL_H

#define EPOLL_SIZE 5000


int epoll_init(int epoll_size);
void epoll_event_addfd(int epollfd, int fd, bool enable_et);

#endif //EPOLL7_EPOLL_H
