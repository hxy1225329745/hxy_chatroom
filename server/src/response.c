//
// Created by 12253 on 2020/1/15.
//

#include <stdio.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "response.h"
#include "server.h"

void send_welcome_message(int fd) {
    printf("before send welcome\n");
    char message[BUF_SIZE];
    bzero(message, BUF_SIZE);

    sprintf(message, SERVER_WELCOME, fd);

    int ret = send(fd, message, BUF_SIZE, 0);
    if(ret < 0) { perror("send1 error"); exit(-1); }

    printf("send_welcome_message success\n");
}

char* recv_message(arraylist *l, int clientfd) {
    char buf[BUF_SIZE];
    static char message[BUF_SIZE + sizeof(SERVER_MESSAGE)];
//    bzero(buf, BUF_SIZE);
    bzero(message, BUF_SIZE + sizeof(SERVER_MESSAGE));
    printf("message1 = %s\n", message);
    printf("read from client(clientID = %d)\n", clientfd);

    int len = recv(clientfd, buf, BUF_SIZE, 0);

    if (len < 0) {
        perror("recv error");
        exit(-1);
    }
    else if (len == 0) {
        close(clientfd);
        int index = arraylist_find(l, clientfd);
        printf("index1(%d)\n", index);
        arraylist_remove(l, index);
        return message;
    }

    sprintf(message, SERVER_MESSAGE, clientfd, buf);
    printf("message2 = %s\n", message);

    return message;
}

void send_broadcast_message(int clientfd, arraylist *client_list, char *message) {
    int stop = arraylist_find(client_list, clientfd);

    for (int i = 0; i < client_list->size; i++) {
        if (i != stop) {
            int ret = send(*(int*)client_list->body[i], message, BUF_SIZE, 0);
            if (ret < 0) {
                perror("error");
                exit(-1);
            }
        }
    }

}



