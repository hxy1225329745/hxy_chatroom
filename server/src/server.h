//
// Created by 12253 on 2020/1/15.
//

#ifndef EPOLL_SERVER_H
#define EPOLL_SERVER_H
#include "arraylist.h"

#define CAUTION "There is only one in the chat room!"

struct server{
    int sockfd;
    short port;
    arraylist *client_list;
};
typedef struct server server;

#endif //EPOLL_SERVER_H
