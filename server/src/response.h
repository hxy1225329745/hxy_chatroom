//
// Created by 12253 on 2020/1/15.
//

#ifndef EPOLL_STARTSERVER_H
#define EPOLL_STARTSERVER_H

#include "arraylist.h"
#include "server.h"


#define BUF_SIZE 0xFFFF
#define SERVER_MESSAGE "ClientID %d say >> %s"
#define SERVER_WELCOME "Welcome you join  to the chat room! Your chat ID is: Client #%d"


void send_welcome_message(int fd);

void send_broadcast_message(int clientfd, arraylist *client_list, char *message);

char* recv_message(arraylist *l, int clientfd);

#endif //EPOLL_STARTSERVER_H
