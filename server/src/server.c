//
// Created by 12253 on 2020/1/15.
//
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <string.h>
#include "server.h"
#include "epoll.h"
#include "response.h"

static server* server_init() {
    server *serv;
    serv = malloc(sizeof(*serv));
    serv->client_list = arraylist_create();
    serv->port = 8888;
    serv->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    return serv;
}

static void bind_and_listen(server *serv) {
    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof serv_addr);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_port = htons(serv->port);

    if (serv->sockfd < 0) {
        perror("socket");
        exit(1);
    }

    if (bind(serv->sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        perror("bind");
        exit(1);
    }

    if (listen(serv->sockfd, 10) < 0) {
        perror("listen");
        exit(1);
    }
}

static int server_accept(server *serv) {
    struct sockaddr_in addr;
    socklen_t addr_len = sizeof(addr);

    int acceptfd = accept(serv->sockfd, (struct sockaddr *) &addr, &addr_len);
    if (acceptfd < 0) {
        perror("accept");
        exit(-1);
    }

    int *clientfd = malloc(sizeof(int));
    memcpy(clientfd, &acceptfd, sizeof(int));

    printf("client connection from: %s : % d(IP : port), clientfd = %d \n",
           inet_ntoa(addr.sin_addr),
           ntohs(addr.sin_port),
           *clientfd);

    arraylist_add(serv->client_list, clientfd);

    return *clientfd;
}


static void wait_accept(server *serv) {
    static struct epoll_event events[EPOLL_SIZE];
    char *message;

    int epfd = epoll_init(EPOLL_SIZE);

    epoll_event_addfd(epfd, serv->sockfd, true);

    while (1) {
        int epoll_events_count = epoll_wait(epfd, events, EPOLL_SIZE, -1);
        if (epoll_events_count < 0) {
            perror("epoll failure");
            break;
        }

        printf("epoll_events_count = %d\n", epoll_events_count);

        for (int i = 0; i < epoll_events_count; ++i) {
            int eventfd = events[i].data.fd;

            if (eventfd == serv->sockfd) {
                int clientfd = server_accept(serv);

                epoll_event_addfd(epfd, clientfd, true);

                printf("Add new clientfd = %d to epoll\n", clientfd);
                printf("Now there are %d clients in the chat room\n", serv->client_list->size);
                printf("welcome message\n\n");

                send_welcome_message(clientfd);

            } else {
                if (serv->client_list->size == 1) {
                    int ret = send(eventfd, CAUTION, strlen(CAUTION), 0);
                    if (ret < 0) {perror("send2 error"); exit(-1);}
                } else {
                    message = recv_message(serv->client_list, eventfd);
                    if (*message > 0) {
                        send_broadcast_message(eventfd, serv->client_list, message);
                    }
                }
            }
        }
    }
    close(serv->sockfd);
    close(epfd);
}


static void start_server(server *serv) {
    bind_and_listen(serv);

    wait_accept(serv);
}

int main(int argc, char** argv) {
    server* serv;
    serv = server_init();

    start_server(serv);
}