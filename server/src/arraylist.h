//
// Created by 12253 on 2020/1/15.
//

#ifndef EPOLL_ARRAYLIST_H
#define EPOLL_ARRAYLIST_H

struct Arraylist {
    unsigned int size;
    unsigned int capacity;
    void **body;
};

typedef struct Arraylist arraylist;
arraylist* arraylist_create();
void arraylist_append(arraylist* l);
void arraylist_add(struct Arraylist *l, void* item);
void* arraylist_get(arraylist *l, unsigned int index);
int arraylist_find(arraylist *l, int content);
void arraylist_remove(arraylist *l, unsigned int index);

#endif //EPOLL_ARRAYLIST_H
